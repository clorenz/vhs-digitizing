#!/bin/bash

DESTDIRBASE="/mnt/nas/video/"
CRF=20

usage() {
  echo "Usage: $0 --sourcefile <sourcefile> --destdir <destdir> [--destfile <destfile>] [--from <from>] [--to <to>]"
  echo "      [--title <title>] [--year <year>] [--episode <episode>]"
  echo "      [--crf <crf>]"
  echo "       <from> and <to> can be in format <seconds> or <hh:mm:ss>"
  echo "            Either both or none of those two arguments must be set"
  echo "       <destdir> is relative to $DESTDIRBASE"
  echo "       <episode> shall be in format s<season>e<id>, e.g. s1998e05"
  echo "       <crf> is the constant rate factor for H.264 encoding."
  echo "             Range is 0-51, where 0 is losless. 18 is visually "
  echo "             losless, but 20 is sufficient for VHS Longplay and "
  echo "             therefor taken as default. An increasment"
  echo "             of 6 leads to about half the file size / bitrate."
}


TEMP=`getopt -o h --long sourcefile:,destdir:,destfile:,from:,to:,title:,year:,episode:,crf: -n 'mpeg2-to-video' -- "$@"`
eval set -- "$TEMP"

while true ; do
  case "$1" in
    --sourcefile)
      case "$2" in
        "") shift 2 ;;
        *) SOURCEFILE="$2" ; shift 2 ;;
      esac ;;
    --destdir)
      case "$2" in
        "") shift 2 ;;
        *) DESTDIR="$2" ; shift 2 ;;
      esac ;;
    --destfile)
      case "$2" in
        "") shift 2 ;;
        *) DESTFILE="$2" ; shift 2 ;;
      esac ;;
    --title)
      case "$2" in
        "") shift 2 ;;
        *) TITLE="$2" ; shift 2 ;;
      esac ;;
    --from)
      case "$2" in
        "") shift 2 ;;
        *) FROM="$2" ; shift 2 ;;
      esac ;;
    --to)
      case "$2" in
        "") shift 2 ;;
        *) TO="$2" ; shift 2 ;;
      esac ;;
    --year)
      case "$2" in
        "") shift 2 ;;
        *) YEAR="$2" ; shift 2 ;;
      esac ;;
    --episode)
      case "$2" in
        "") shift 2 ;;
        *) EPISODE="$2" ; shift 2 ;;
      esac ;;
    --crf)
      case "$2" in
        "") shift 2 ;;
        *) CRF=$2 ; shift 2 ;;
      esac ;;
    --) shift ; break ;;
    -h) usage ; exit 0 ;;
    *) usage ; exit 1 ;;
  esac
done

if [ -z "$SOURCEFILE" ] ; then
  usage;
  exit 1;
fi

if [ -z "$DESTDIR" ] ; then
  usage;
  exit 1;
else
  DESTDIR="$DESTDIRBASE$DESTDIR"
fi

if [ -z "$DESTFILE" ] ; then
  DESTFILE=$SOURCEFILE
fi

#if [ ! -z "$TITLE" ] ; then
#  TITLEOPTS="-metadata title=\"$TITLE\""
#fi
if [ ! -z "$YEAR" ] ; then
  TITLEOPTS="$TITLEOPTS -metadata year=\"$YEAR\""
fi
if [ ! -z "$EPISODE" ] ; then
  TITLEOPTS="$TITLEOPTS -metadata episode_id=\"$EPISODE\""
fi

if [ -z "$FROM" -a -n "$TO" ] ; then
  usage;
  exit 1;
fi

if [ -n "$FROM" -a -z "$TO" ] ; then
  usage;
  exit 1;
fi

if [ -n "$FROM" -a -n "$TO" ] ; then
  CUTOPTS="-ss $FROM -to $TO"
fi

filename=$(basename "$DESTFILE")
extension="${filename##*.}"
DESTFILE="$DESTDIR/${filename%.*}".mp4

echo "Transforming '$SOURCEFILE' -> '$DESTFILE'"
echo "     CRF=$CRF"
if [ ! -z "$TITLE" ] ; then
  echo "     Title=$TITLE"
fi
if [ ! -z "$TITLEOPTS" ] ; then
  echo "     Metadata=$TITLEOPTS"
fi
if [ -n "$FROM" -a -n "$TO" ] ; then
  echo "     Time range=$FROM to $TO"
fi

# Select streams 0:0 (Audio) and 0.2 (Video). Set H.264 level 3.1
STREAMOPTS="-map 0:0 -map 0:2 -level 3.1"       # map 0:2 map 0:1
# Audio is already mp3 encoded in the sources
AUDIOOPTS="-c:a:0 copy"           # a:2
# Video settings (Video = Stream 1), try to produce as small and as good as possible files
VIDEOOPTS="-c:v:2 libx264 -preset slow -crf $CRF -tune film -flags +ilme+ildct "   # v:1
# sharpen, remove VHS drum switching noise at bottom and white ligne at top
FILTEROPTS="-vf drawbox=y=ih-h:w=0:h=10:t=max,drawbox=h=2:t=max,drawbox=w=11:t=max,drawbox=x=iw-w:w=15:t=max,yadif=1"

if [ -z "$TITLE" ] ; then
  ffmpeg -i "$SOURCEFILE" $CUTOPTS $STREAMOPTS $TITLEOPTS $VIDEOOPTS $AUDIOOPTS $FILTEROPTS "$DESTFILE"
else
  # metadata title cannot be passed as variable
  ffmpeg -i "$SOURCEFILE" $CUTOPTS $STREAMOPTS -metadata title=\""$TITLE"\" $TITLEOPTS $VIDEOOPTS $AUDIOOPTS $FILTEROPTS "$DESTFILE"
fi
