# vhs-digitizing

Scripts for digitizing VHS videos

```vhs-to-mpeg2``` is used to record a VHS video in best possible quality to an mpeg2 filename
```mpeg2-to-video``` is used to convert the mpeg2 or parts of it to a TV- and DVD usable mp4 file
